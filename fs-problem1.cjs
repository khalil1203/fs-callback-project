const fs = require('fs');


// Creating function to create and delete .json files simultaneously!!!
const create_delete_json = (directory, random_num) => {
    let count = 0;
    //Creating a directory using fs module!!
    fs.mkdir(directory, (err) => {
        if (err) {
            return console.error(err);

        }
        else {
            console.log("Directory is created");

            // create only limited number of files!!!
            for (let index = 0; index < random_num; index++) {

                let data = JSON.stringify({
                    id: index,
                    name: `random${index}`
                });

                // Creating new file using fs module in the directory!!!
                fs.writeFile(`${directory}/random${index}.json`, data, (err) => {
                    if (err) {
                        console.error(`Error creating file `);
                    } else {
                        count++;
                        console.log(`File created : random${index}.json`);
                        if (count >= random_num) {
                            delete_files(directory);
                        }

                    }
                });

            }

        }
    });


    // Deleting function to delete the files!!!
    function delete_files(directory) {

                for (let index = 0; index < random_num; index++) {
                    const filepath = `${directory}/random${[index]}.json`;

                    //Deleting the file!!!
                    fs.unlink(filepath, (err) => {
                        if (err) {
                            console.error(`Error deleting file ${index}: ${err}`);
                        }
                        else {
                            console.log(`File deleted: ${filepath}`);
                        }
                    })
                }

                fs.rmdir(directory, (err) => {

                    if (err) {
                        return console.log("error occurred in deleting directory", err);
                    }

                    console.log("Directory deleted successfully");
                });
    }
}


module.exports = create_delete_json;


