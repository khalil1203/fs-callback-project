
const fs = require('fs');
const answer2 = function (lipsum, callback) {
    // Step 1: Read the given file lipsum.txt
    fs.readFile(lipsum, 'utf8', (err, data) => {
        if (err) {
            console.error('Error :', err);
            return callback(err);
        }

        else {

            // Step 2: Convert the content to uppercase & write to a new file
            const uppercaseData = data.toUpperCase();
            const uppercasePath = '../Uppercase.txt';

            fs.writeFile(uppercasePath, uppercaseData, (err) => {
                if (err) {
                    console.error('Error while writing to newfile1.txt:', err);
                    return;
                }
                else {

                    updateFile(uppercasePath + "\n");

                    console.log("Uppercase.txt file is created successfully!!!");

                    // Step 3: Read the new file, convert to lowercase, split into sentences & write to a new file
                    fs.readFile(uppercasePath, 'utf8', (err, data) => {
                        if (err) {
                            console.error('Error while reading Uppercase.txt:', err);
                            return;
                        }
                        else {

                            const lowercaseData = data.toLowerCase();
                            const splited = lowercaseData.split('. ');

                            const lowercasePath = '../Lowercase.txt';

                            fs.writeFile(lowercasePath, splited.join('\n'), (err) => {
                                if (err) {
                                    console.error('Error while writing to newfile2.txt:', err);
                                    return;
                                }

                                else {

                                    console.log("Lowercase.txt file is created successfully!!!");
                                    updateFile(lowercasePath + "\n");
                                    // Step 4: Read the new file, sort the content & write to a new file


                                    fs.readFile(lowercasePath, 'utf8', (err, lowerdata) => {
                                        if (err) {
                                            console.error('Error while reading Lowercase.txt:', err);
                                            return;
                                        }
                                        else {

                                            const sortedData = lowerdata.split('\n').sort().join('\n');
                                            const sortedPath = '../Sorted.txt';

                                            fs.writeFile(sortedPath, sortedData, (err) => {
                                                if (err) {
                                                    console.error('Error while writing to Sorted.txt:', err);
                                                    return;
                                                }
                                                else {

                                                    console.log("Sorted.txt file is created successfully!!!");

                                                    updateFile(sortedPath);



                                                    // Step 5: Read filenames.txt and delete all the new files mentioned in that list
                                                    fs.readFile('../filenames.txt', 'utf8', (err, filenamesData) => {
                                                        if (err) {
                                                            console.error('Error while reading filenames.txt:', err);
                                                            return;
                                                        }

                                                        else {
                                                            const filenames = filenamesData.split('\n');


                                                            filenames.map((filename) => {
                                                                fs.unlink(filename, (err) => {
                                                                    if (err) {
                                                                        console.error(`Error while deleting ${filename}:`, err);
                                                                        return;
                                                                    }
                                                                    else {
                                                                        console.log(`${filename} is deleted successfully.`);
                                                                    }
                                                                });
                                                            })
                                                            fs.unlink("../filenames.txt", (err) => {
                                                                if (err) {
                                                                    console.error(`Error while deleting filenames.txt:`, err);
                                                                    return;
                                                                }
                                                                else {
                                                                    console.log(`filenames.txt is deleted successfully.`);
                                                                }
                                                            });

                                                        }

                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

}




function updateFile(data) {
    fs.appendFile("../filenames.txt", data, (err) => {
        if (err) {
            console.error('Error while writing to filenames.txt:', err);
            return;
        }
        else {
            console.log(`${data} added to filenames.txt`);
        }
    });
}

module.exports = answer2;